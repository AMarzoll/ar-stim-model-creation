#%% Train model on output of audio wavelet convolutions created by Matlab
from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import random
import scikeras #sklearn wrapper für nicht-sequentielle Modelle

### DATA PROCESSING ###

train_path = r"..\ML_output\ML_traindata_2021_09_18_16_19.csv"
test_path = r"..\ML_output\ML_evaldata_2021_09_18_16_19.csv"

train = pd.read_csv(train_path, header=0, sep=',', dtype=np.float32)
test = pd.read_csv(test_path, header=0, sep=',', dtype=np.float32)


#%% ### DECREASE SIZE FOR QUICK TESTING ###

if False:
    n_samples_train = 5000000
    n_samples_test = 1000000
    train = train.iloc[random.sample(range(0, len(train)), n_samples_train)]
    test = test.iloc[random.sample(range(0, len(test)), n_samples_test)]


#%% ### LABEL GROUPS ###

def appendNums(string, num_range):
    num_range = list(num_range)
    strList = []
    for i in num_range:
        strList.append(string + str(i))
    return strList

#feature names for wavelet convolution input: contains wavelets tuned to neighboring tones, 50c-offset (halfNeighbor), overtones, half/quarter/...-fundamental frequencies

featList_neighbors = appendNums("neighboringTones_", range(12,0,-1))
featList_halfNeighbors = appendNums("halfNeighbor_", range(2,0,-1))
featList_overtones = appendNums("overtone_", range(6,0,-1))
featList_halfFZero = appendNums("halfF0_", range(2,0,-1))
featList_intPlusHalfFZero = appendNums("integerPlusHalfF0_", range(4,0,-1))
featList_loudness = ["loudness_factor"]

ML_features = pd.Index(featList_neighbors + 
                       featList_halfNeighbors + 
                       featList_overtones + 
                       featList_halfFZero + 
                       featList_intPlusHalfFZero + 
                       featList_loudness)

def appendBackSegmentNames(seq, nBackSegments=5):
    seq = list(seq)
    ML_features_app = list()
    for i in range(-nBackSegments, 1):
        for string in seq:
            ML_features_app.append(string+"_t"+str(i))
    return ML_features_app

ML_features_all = appendBackSegmentNames(pd.Index(featList_neighbors + 
                                                  featList_halfNeighbors + 
                                                  featList_overtones + 
                                                  featList_halfFZero + 
                                                  featList_intPlusHalfFZero)
                                         )
                                       
ML_loudness_all = appendBackSegmentNames(pd.Index(featList_loudness))
featList_toneDiff = appendNums("toneDiff_", range(6,0,-1))
featList_noiseType = list(["noise_type"])

diagnostic_features = pd.Index(featList_toneDiff + featList_noiseType)

train_diagnostic = pd.concat([train.pop(x) for x in diagnostic_features], axis=1)
test_diagnostic  = pd.concat([test.pop(x) for x in diagnostic_features], axis=1)


#%% ### NORMALIZATION ###
#TODO: use sklearn transformers instead of doing this manually

def normalizeFeatureSet(featureIndices, nSamples=100000):
    feat_means = train[featureIndices].iloc[random.sample(range(0, len(train)), nSamples)].mean().mean()
    feat_stds = train[featureIndices].iloc[random.sample(range(0, len(train)), nSamples)].std(axis=0).mean()
    train[featureIndices] = (train[featureIndices] - feat_means) / feat_stds
    test[featureIndices] = (test[featureIndices] - feat_means) / feat_stds
    return [feat_means, feat_stds]
    
features_mean, features_std = normalizeFeatureSet(ML_features_all)
loudness_mean, loudness_std = normalizeFeatureSet(ML_loudness_all)


#%% ### REMOVE FEATURES ###

def remFeature(strList):
    delList = list()
    colNames = list(train)
    for pattern in strList:
        for string in colNames:
            if pattern in string:
                delList.append(string)
    for string in delList:
        train.pop(string)
        test.pop(string)

remFeature(featList_neighbors[0:0] + #0/12 insg.
           featList_overtones[:-2] + #4/6 insg.
           featList_halfNeighbors[:] + #0/2 insg.
           featList_halfFZero[:-2] + #0/2 insg.
           featList_intPlusHalfFZero[:-2] + #2/4 insg.
           featList_loudness[0:0]) #0/1 insg. 


#%% ### EXPLORATION ###

import random
train = train.iloc[random.sample(range(0,len(train)), 100000)]

train.hist(bins=50, figsize=(20,15))
corr_matrix = train.corr()
print(corr_matrix["target_tone_present"].sort_values(ascending=False))
print(corr_matrix["overtone_1"].sort_values(ascending=False))

from pandas.plotting import scatter_matrix
scatter_matrix(train, figsize=(25,20))



#%% ### SEPARATE LABELS ###

train_labels = pd.DataFrame(train.pop('target_tone_present'))
test_labels = pd.DataFrame(test.pop('target_tone_present'))


#%% ### CREATE ADDITIONAL LABELS ###
# (hopefully) increase selectivity by having the model learn when a tone is present, but merely off by one half-tone step

def offByOneLabelAdder(df, df_labels, df_diagnostic):
    lower = list(np.where(df_diagnostic[featList_toneDiff] == -1))[0]
    upper = list(np.where(df_diagnostic[featList_toneDiff] ==  1))[0]
    targets = list(np.where(df_diagnostic[featList_toneDiff] == 0))[0]
    hard_case_idx = np.concatenate((lower, upper))
    hard_case_idx = np.setdiff1d(hard_case_idx, targets)
    df_labels['is_off_by_one'] = 0.0
    df_labels['is_off_by_one'].iloc[hard_case_idx] = 1.0

offByOneLabelAdder(train, train_labels, train_diagnostic)
offByOneLabelAdder(test, test_labels, test_diagnostic)


#%% ### REMOVE INVALID DATAPOINTS ###

na_idx = train.isna().any(axis=1)
train = train[~na_idx]
train_labels = train_labels[~na_idx]
train_diagnostic = train_diagnostic[~na_idx]

#%% ### CREATE MODEL ARCHITECTURE ###

def create_model(meta, learning_rate, loss_weight_target):
    
    input_layer = tf.keras.layers.Input(shape=meta['n_features_in_'], name="Input1")
    
    hidden1_layer = tf.keras.layers.Dense(128,
                                       activation="relu",
                                       name="Hidden1")(input_layer)
    dropout1_layer = tf.keras.layers.Dropout(rate=0.0,
                                             name="Dropout1")(hidden1_layer)
    hidden2_layer = tf.keras.layers.Dense(128,
                                       activation="relu",
                                       name="Hidden2")(dropout1_layer)
    dropout2_layer = tf.keras.layers.Dropout(rate=0.0,
                                             name="Dropout2")(hidden2_layer)
    hidden3_layer = tf.keras.layers.Dense(128,
                                       activation="relu",
                                       name="Hidden3")(dropout2_layer)
    dropout3_layer = tf.keras.layers.Dropout(rate=0.0,
                                             name="Dropout3")(hidden3_layer)
    hidden4_layer = tf.keras.layers.Dense(128,
                                       activation="relu",
                                       name="Hidden4")(dropout3_layer)
    dropout4_layer = tf.keras.layers.Dropout(rate=0.0,
                                             name="Dropout4")(hidden4_layer)
    hidden5_layer = tf.keras.layers.Dense(128,
                                       activation="relu",
                                       name="Hidden5")(dropout4_layer)
    dropout5_layer = tf.keras.layers.Dropout(rate=0.0,
                                             name="Dropout5")(hidden5_layer)
    output1_layer = tf.keras.layers.Dense(units=1,
                                          activation="sigmoid",
                                          name="Output1")(dropout5_layer)
    output2_layer = tf.keras.layers.Dense(units=1,
                                          activation="sigmoid",
                                          name="Output2")(dropout4_layer)
                   
    model = tf.keras.Model(inputs=[input_layer], outputs=[output1_layer, output2_layer])            
  
    model.compile(optimizer = tf.keras.optimizers.Adam(lr = learning_rate),
                  loss = [tf.keras.losses.BinaryCrossentropy(), tf.keras.losses.BinaryCrossentropy()], 
                  loss_weights = [loss_weight_target, 1-loss_weight_target],
                  metrics=[tf.keras.metrics.BinaryCrossentropy(),
                           tf.keras.metrics.BinaryAccuracy(name='accuracy', threshold=0.5),
                           tf.keras.metrics.Precision(thresholds=0.5, name='precision'),
                           tf.keras.metrics.Recall(thresholds=0.5, name='recall'),
                           tf.keras.metrics.AUC(name='AUC')])

    return model

from typing import List

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import LabelEncoder


class MultiOutputTransformer(BaseEstimator, TransformerMixin):

    def fit(self, y):
        y_tgt, y_obo = y[:, 0], y[:, 1]
        # Create internal encoders to ensure labels are 0, 1, 2...
        self.tgt_encoder_ = LabelEncoder()
        self.obo_encoder_ = LabelEncoder()
        # Fit them to the input data
        self.tgt_encoder_.fit(y_tgt)
        self.obo_encoder_.fit(y_obo)
        # Save the number of classes
        self.n_classes_ = [
            self.tgt_encoder_.classes_.size,
            self.obo_encoder_.classes_.size,
        ]
        # Save number of expected outputs in the Keras model
        # SciKeras will automatically use this to do error-checking
        self.n_outputs_expected_ = 2
        return self

    def transform(self, y) -> List[np.ndarray]:
        y_tgt, y_obo = y[:, 0], y[:, 1]
        # Apply transformers to input array
        y_tgt = self.tgt_encoder_.transform(y_tgt)
        y_obo = self.obo_encoder_.transform(y_obo)
        # Split the data into a list
        return [y_tgt, y_obo]

    def inverse_transform(self, y_pred_proba: List[np.ndarray], return_proba: bool = False) -> pd.DataFrame:
        if return_proba:
            return pd.DataFrame(np.column_stack(y_pred_proba, axis=1))
        # Get class predictions from probabilities
        y_pred_tgt = (y_pred_proba[0] > 0.5).astype(int).reshape(-1, )
        y_pred_obo = (y_pred_proba[1] > 0.5).astype(int).reshape(-1, )
        # Pass back through LabelEncoder
        y_pred_tgt = self.tgt_encoder_.inverse_transform(y_pred_tgt)
        y_pred_obo = self.obo_encoder_.inverse_transform(y_pred_obo)
        return pd.DataFrame(np.column_stack([y_pred_tgt, y_pred_obo]))

    def get_metadata(self):
        return {
            "n_classes_": self.n_classes_,
            "n_features_in_": len(train.columns),
            "n_outputs_expected_": self.n_outputs_expected_,
            }


from sklearn.metrics import accuracy_score
from scikeras.wrappers import KerasClassifier

class MultiOutputClassifier(KerasClassifier):

    @property
    def target_encoder(self):
        return MultiOutputTransformer()

    @staticmethod
    def scorer(y_true, y_pred, **kwargs):
        y_pred = y_pred.to_numpy()
        y_tgt, y_obo = y_true[:, 0], y_true[:, 1]
        y_pred_tgt, y_pred_obo = y_pred[:, 0], y_pred[:, 1]
        # Keras by default uses the mean of losses of each outputs, so here we do the same
        #return np.mean([accuracy_score(y_tgt, y_pred_tgt), accuracy_score(y_obo, y_pred_obo)])
        return accuracy_score(y_tgt, y_pred_tgt)
    

from tensorflow.keras.utils import Sequence

class data_generator(Sequence):
    def __init__(self, batch_size):
        self.batch_size = batch_size
        self.n = len(train)
        
    def __getitem__(self, index):
        X = train.iloc[index * self.batch_size : (index+1)*self.batch_size]
        y = train_labels.iloc[index * self.batch_size : (index+1)*self.batch_size]
        w = train_weights[index * self.batch_size : (index+1)*self.batch_size]
        return X, (y['target_tone_present'], y['is_off_by_one']), w
    
    def __len__(self):
        return self.n // self.batch_size
    
    def on_epoch_end(self):
        pass

physical_devices = tf.config.list_physical_devices('GPU') 
tf.config.experimental.set_memory_growth(physical_devices[0], True)

units = [256, 128, 64]
learning_rate = 0.03
epochs = 5
batch_size = 2**14
l2_reg_rate = 0.001
loss_weights = [0.7, 0.3]
dropout_rate = 0.0
classification_threshold = 0.5
loss_weights=[0.6, 0.4]
metrics=[tf.keras.metrics.BinaryCrossentropy(),
         tf.keras.metrics.BinaryAccuracy(name='accuracy', threshold=0.5),
         tf.keras.metrics.Precision(thresholds=0.5, name='precision'),
         tf.keras.metrics.Recall(thresholds=0.5, name='recall'),
         tf.keras.metrics.AUC(name='AUC')]

train_weights = train_labels['is_off_by_one'].to_numpy()
train_weights = train_weights * 5
zero_idx = np.where(train_weights == 0.0)
train_weights[zero_idx] = 1.0
train_weights = pd.Series(train_weights)

clf = MultiOutputClassifier(model=create_model,
                            fit__epochs=50,
                            fit__batch_size=2**13,
                            model__learning_rate=0.05,
                            model__loss_weight_target=1.0)
from skopt import BayesSearchCV
from skopt.space import Real, Integer

space = {"model__learning_rate": Real(1e-5, 1e-2, prior="log-uniform"),
         "model__loss_weight_target": Real(0.4, 0.6, prior="uniform"),
         "fit__batch_size": Integer(2**8, 2**12, prior="log-uniform")}

bs = BayesSearchCV(clf, space, n_iter=5, cv=3)
#dg = data_generator(batch_size=2**13)
bs.fit(train, train_labels)
#bs.fit(train, train_labels, sample_weight=train_weights)

model = bs.best_estimator_.model_

print("\n Evaluate the new model against the test set:")
model.evaluate(x=test, 
               y=(test_labels['target_tone_present'], test_labels['is_off_by_one']), 
               batch_size=batch_size)


#%% ### EXAMINE HARD CASES ###

def eval_model_by_cond(idx, case_name_string):
    test_cond = test.iloc[idx]
    test_labels_cond = test_labels.iloc[idx]
    print("\n Evaluate " + case_name_string + " cases only: ")
    model.evaluate(x=test_cond,
                   y=(test_labels_cond['target_tone_present'], test_labels_cond['is_off_by_one']),
                   batch_size=batch_size)
#off-by-one-halftone cases
lower = list(np.where(test_diagnostic[featList_toneDiff] == -1))[0]
upper = list(np.where(test_diagnostic[featList_toneDiff] ==  1))[0]
targets = list(np.where(test_diagnostic[featList_toneDiff] == 0))[0]
obo_case_idx = np.concatenate((lower, upper))
obo_case_idx = np.setdiff1d(obo_case_idx, targets)
eval_model_by_cond(obo_case_idx, "off-by-one-halftone")

#by-noise cases
no_noise_idx = list(np.where(test_diagnostic[featList_noiseType] == 1))[0]
eval_model_by_cond(no_noise_idx, "noise-free")

white_noise_idx = list(np.where(test_diagnostic[featList_noiseType] == 2))[0]
eval_model_by_cond(white_noise_idx, "white noise")

pink_noise_idx = list(np.where(test_diagnostic[featList_noiseType] == 3))[0]
eval_model_by_cond(pink_noise_idx, "pink noise")

#other-octave cases
upper_octave_idx = list(np.where(test_diagnostic[featList_toneDiff] ==  12))[0]
lower_octave_idx = list(np.where(test_diagnostic[featList_toneDiff] == -12))[0]
other_octave_idx = np.concatenate((lower_octave_idx, upper_octave_idx))
other_octave_idx = np.setdiff1d(other_octave_idx, targets)
eval_model_by_cond(other_octave_idx, "other octave")


#%% ### EXPORT MODEL ###

model.save(filepath = ".\\mein_modell.h5", save_format = 'h5')

converter = tf.lite.TFLiteConverter.from_keras_model(model)
tflite_model = converter.convert()
open(".\mein_modell.tflite", "wb").write(tflite_model)
