# Data processing and model training for visual stimulation with augmented-reality glasses

Contains Matlab scripts to convert sound files with corresponding label data (indicating the presence of tones on the Western musical scale) into input for a neural network model. Raw sound data were recorded with a Casio keyboard and an online synthesizer. Sound data are already preprocessed by applying wavelet convolution to them, with wavelets tuned to different frequencies of interest (neighboring tones, overtones, undertones, etc.). This method has the potential advantage of requiring the training of only one model, with flexible control over how many/which tones to detect (instead of a multioutput model that is relatively fixed with respect to the detectable tones from raw audio input) and also reduces the size of the training data for during model fitting. Also included is a Python script to train a TensorFlow neural network model given these input data to detect the presence of a tone of interest. This model also takes into account the audio data from preceding epochs (one epoch being 33.3 ms). The resulting model gets converted into a TensorFlow lite (tflite) model for use during visual stimulation on the augmented-reality glasses, the Android app for which can be found here: https://gitlab.com/AMarzoll/augmentedrealitystimulation

## Features

- Implements wavelet convolution with complex-valued Morlet wavelets (Cohen, 2014, MIT Press)
- Includes a helper script to label .wav files with graphical assistance
- Data augmentation through the addition of white and pink noise to raw sound data
- The actual TF model is a sequential model with dropout layers. Increased selectivity for the target tone was reached by additionally forcing the network to detect whether the input represents a tone off by one half-tone step from the target tone

## Structure

- Matlab scripts for data generation and transformation are located in `/ML_Matlab_data_generation`
- Python script for model training is located in `/ML_Python_model_training`
