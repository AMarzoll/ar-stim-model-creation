%% Create a map of tones (Western scale) and their frequencies in Hz
function [toneMap, toneNames, freqMap, frex] = makeToneMap()

noteLetters = ["C","C#","D","D#","E","F","F#","G","G#","A","A#","B"];
octaves = 0:8;

idx = 1;
for curOctave = octaves
    for curLetter = noteLetters
        for curSuffix = ["", "+50c"]
            toneNames(idx) = strcat(curLetter,num2str(curOctave),curSuffix);
            idx = idx + 1;
        end
    end
end

f0 = 440; %Hz
halfStepsToC0 = 57;
ns = [-halfStepsToC0 : 0.5 : -halfStepsToC0+(numel(toneNames)/2)-0.5];
frex = f0 * (2^(1/12)).^ns;

toneMap = containers.Map(toneNames, frex);
freqMap = containers.Map(frex, toneNames);
end