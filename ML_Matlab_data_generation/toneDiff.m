%% Calculate the number of half-tone steps between two given notes on the Western scale
function nSteps = toneDiff(tone1, tone2)

noteLetters = ["C","C#","D","D#","E","F","F#","G","G#","A","A#","B"];
octaves = 0:8;

idx = 1;
for curOctave = octaves
    for curLetter = noteLetters
        toneNames(idx) = strcat(curLetter,num2str(curOctave));
        idx = idx + 1;
    end
end

nSteps = find(toneNames == tone2) - find(toneNames == tone1);

end