%% Helper script to sanity check Wavelet convolution

co = [166,206,227;
      31,120,180;
      178,223,138;
      51,160,44;
      251,154,153;
      227,26,28;
      253,191,111
      255,127,0;
      202,178,214;
      106,61,154;
      255,255,153;
      177,89,40]./255;
set(groot,'defaultAxesColorOrder',co)

music_file = 'soundtest4.mp3';
label_file = 'soundtest4_labels.txt';

cd('./test_data')

[data,fs] = audioread(music_file);

hF = fopen(label_file,'r');
labels = textscan(hF,'%s','delimiter',sprintf('\n'));
fclose(hF);

cd('../')

%% Setup
%requires 'data' and 'fs'
if(size(data,2) == 2)
   temp = sum(data,2);
   temp = temp / max(abs(temp));
   data = temp;
   clear temp
end

%wavelets
oct_c5 = [523.25 554.37 587.33 622.25 659.25 698.46 739.99 783.99 830.61 880.00 932.33 987.77];

frex = oct_c5;

             
legend_labels = {'C5','C#5','D5','D#5','E5','F5','F#5','G5','G#5','A5','A#5','B5'};

n = 100;
for iF = 1:numel(frex)
    wvlt(iF) = Wavelet(frex(iF),n,fs,2048);
end

%% Analyse

for iF = 1:numel(frex)
   w{iF} = conv(data,wvlt(iF).wavelet,'same');
end

%% figures

figure
hold on
for iF = 1:numel(frex)
    p = plot(abs(w{iF}).*frex(iF),'LineWidth',2);
    if iF > 12
        p.LineStyle = '-.';
        if iF > 24
            p.LineStyle = ':';
        end
    end
end
hold off
set(gca,'ColorOrderIndex',1)

legend(legend_labels);

figure
matrix = transpose(abs(cell2mat(w)));
for iF = 1:numel(frex), matrix(iF,:) = matrix(iF,:).*frex(iF); end
imagesc(flipud(log10(matrix)));
set(gca,...
    'YTick',1:numel(frex),...
    'YTickLabels',fliplr(legend_labels));
clear matrix

%% "reconstruct" melody
minDur = round(0.100 * fs);
threshold = 2500;
classifierFun = @(x,f)(AM_thresholdedRunLength(abs(x).*f, threshold, minDur));

rec = zeros(1,numel(data));
time = 0:1/fs:(numel(data)-1)/fs;

for iF = 1:numel(w)
   rng = classifierFun(w{iF},frex(iF));
   rec(rng) = rec(rng) + frex(iF).*abs(w{iF}(rng))'.*sin(2*pi*frex(iF)*time(1,rng));
end


rec = rec / max(abs(rec));

%% bin analysis

binLength = 0.050;
binOverlap = 0.010;
binSamples = round(binLength*fs);
overlapSamples = round(binOverlap*fs);

nBins = ceil(length(data)/(binSamples-overlapSamples));
dataReqLen = binSamples+((nBins-1)*(binSamples-overlapSamples));
data = [data; zeros(dataReqLen - length(data), 1)];

for iF = 1:numel(frex)
    w_binned{iF} = zeros(nBins*binSamples,1);
    for iBin = 1:nBins
        currBinSamples = 1+(iBin-1)*binSamples:iBin*binSamples;
        currDataSamples = 1+((iBin-1)*(binSamples-overlapSamples)) : binSamples+((iBin-1)*(binSamples-overlapSamples));
        w_binned{iF}(currBinSamples) = conv(data(currDataSamples),wvlt(iF).wavelet,'same');
    end
end

figure
matrix = transpose(abs(cell2mat(w_binned)));
for iF = 1:numel(frex), matrix(iF,:) = matrix(iF,:).*frex(iF); end
imagesc(flipud(log10(matrix)));
set(gca,...
    'YTick',1:numel(frex),...
    'YTickLabels',fliplr(legend_labels));
clear matrix

for iBin = 1:nBins
    line([binSamples*iBin binSamples*iBin], [0.5 numel(frex)+0.5], 'Color',[0.3 0.3 0.3])
end









