%% Class implementing complex-valued wavelets with methods for visualization and convolution
classdef Wavelet
    properties
        f {mustBeNumeric}
        n {mustBeNumeric}
        fs {mustBeNumeric}
        s
        time
        wavelet
        lastConvolution = [];
        randFlag = false;
        randStdev = 0.5;
        cleanTaperVal = 0.01;
        dataType = 'double';
    end

    methods
        function obj = Wavelet(f, n, fs, nSamples, randFlag, varargin)
            obj.f = sort(f);
            obj.n = n;
            obj.fs = fs;
            obj.s = n./(2*pi.*f);
            obj.time = -(nSamples/2)/fs : 1/fs : ((nSamples-1)/2)/fs;
            obj.randFlag = randFlag;
            for iF = 1:numel(obj.f)
                if ~randFlag
                    singleWavelets(iF,:) = obj.complexSine(obj.f(iF),obj.time) .* obj.gaussWindow(obj.time,obj.s(iF));
                else
                    singleWavelets(iF,:) = complex(AM_clamp(randn(1,numel(obj.time))*obj.randStdev, -1, 1), AM_clamp(randn(1,numel(obj.time))*obj.randStdev, -1, 1)) .* obj.gaussWindow(obj.time,obj.s(iF));
                end
            end
            obj.wavelet = (sum(singleWavelets, 1) ./ numel(f));
%             if(((abs(real(obj.wavelet(1)))   > obj.cleanTaperVal || ...
%                  abs(real(obj.wavelet(end))) > obj.cleanTaperVal || ...
%                  abs(imag(obj.wavelet(1)))   > obj.cleanTaperVal || ...
%                  abs(imag(obj.wavelet(end))) > obj.cleanTaperVal)) && ...
%                  ~randFlag && ...
%                  obj.f(1) > 0)
%                     warning("Wavelet constructor with parameters f=%.5u, n=%d, fs=%u, nSamples=%u: Wavelet does not taper to zero!",...
%                             obj.f, obj.n, obj.fs, nSamples);
%             end
            
            if nargin == 6
               obj.dataType = varargin{1}; 
               if strfind(obj.dataType, 'int')
                   obj.wavelet = obj.wavelet * double(intmax(obj.dataType));
               end
               obj.wavelet = cast(obj.wavelet, obj.dataType);
            end
            
        end
        function res = getFreq(obj)
            res = obj.f;
        end
        function res = convolve(obj, signal)
            res = conv(signal,obj.wavelet,'same');
        end
        function [res, obj] = convolveAndSave(obj, signal)
            res = conv(signal,obj.wavelet,'same');
            obj.lastConvolution = res;
        end
        function plotConvolution(obj)
            if(isempty(obj.lastConvolution))
                warning('obj.lastConvolution is empty')
                return;
            end
            plot(obj.lastConvolution);
        end
        function plotWavelet(obj)
            figure
            subplot(1,2,1)
            plot(obj.time,real(obj.wavelet))
            title('real part')
            hold on
            line([0 0],[-1 1],'LineStyle',':','Color','k');
            xlabel('time [s]')
            ylabel('wavelet amplitude [a.u.]')
            %ylim([-1 1])
            subplot(1,2,2)
            plot(obj.time,imag(obj.wavelet))
            title('imaginary part')
            hold on
            line([0 0],[-1 1],'LineStyle',':','Color','k');
            xlabel('time [s]')
            ylabel('wavelet amplitude [a.u.]')
            %ylim([-1 1])
            freqCell = mat2cell(obj.f, 1);
            if ~obj.randFlag
                sgtitle(strcat('Complex Morlet Wavelet - f =', sprintf(' %i', freqCell{:}), sprintf(' Hz, n = %i, fs = %i, samples = %i',obj.n,obj.fs,numel(obj.time))));
            else
                sgtitle(sprintf('Complex Morlet Wavelet - white noise, fs = %i, samples = %i',obj.fs,numel(obj.time)))
            end
        end
        function plotWavelet3D(obj)
            figure
            hold on
            line([obj.time(1) obj.time(end)],[0 0], [0 0], 'LineStyle',':','Color','k')
            line([0 0], [-1 1], [0 0], 'LineStyle',':','Color','k')
            line([0 0], [0 0], [-1 1], 'LineStyle',':','Color','k')
            for i = 1:numel(obj.time)
               plot3(obj.time(i),real(obj.wavelet(i)),imag(obj.wavelet(i)), 'Marker','.', ...
               'Color', [AM_clamp(0.5 + 0.5*real(obj.wavelet(i)) - 0.5*imag(obj.wavelet(i)), 0, 1), ...
                         AM_clamp(0.5 - 0.5*real(obj.wavelet(i)) - 0.5*imag(obj.wavelet(i)), 0, 1), ...
                         AM_clamp(0.5 + 0.5*imag(obj.wavelet(i)), 0, 1)]);
               hold on
            end
            xlabel('time [s]')
            ylabel('wavelet amplitude - real part [a.u.]')
            zlabel('wavelet amplitude - imaginary part [a.u.]')
            freqCell = mat2cell(obj.f, 1);
            if ~obj.randFlag
                sgtitle(strcat('Complex Morlet Wavelet - f =', sprintf(' %i', freqCell{:}), sprintf(' Hz, n = %i, fs = %i, samples = %i',obj.n,obj.fs,numel(obj.time))));
            else
                sgtitle(sprintf('Complex Morlet Wavelet - white noise, fs = %i, samples = %i',obj.fs,numel(obj.time)))
            end
        end
        function makeWavelet3DVideo(obj)
            
            plotWavelet3D(obj);
            f_anim = gcf;
            ax_anim = gca;
            set(f_anim,'Units','centimeters','Position',[5 5 23 23]);
            set(ax_anim,'Box','on','XGrid','on','YGrid','on','ZGrid','on');
            
            videoHandle = VideoWriter('wavelet_3D.avi');
            videoHandle.Quality = 95;
            videoHandle.FrameRate = 30;
            open(videoHandle);
            for k = 135:360+135
                set(ax_anim,'View', [k,35]);
                set(ax_anim,'ZLim', [-1 1], 'YLim', [-1 1])
                axis square
                drawnow;
                pause(0.01);
                frame = getframe(f_anim);
                writeVideo(videoHandle, frame);
            end
            close(videoHandle);
        end
    end

    methods (Static)
        function out = gaussWindow(t,s)
           out = exp(-t.^2./(2*s^2));
        end
        function out = complexSine(f,t)
           out = exp(1i*2*pi*f.*t);
        end
    end
end