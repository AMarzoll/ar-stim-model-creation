%% Graphical helper script to label recorded .wav files
%% phase 0 - preparation

folderName = "sequencer_data";
filename_original = "onlinesequencer.net_xylophone_G3_F6_5.ogg";
[data, fs] = audioread(fullfile(".",folderName,filename_original));
if(size(data, 2) == 2)
    data = mean(data, 2);
end

%data = 2 * (data - min(data)) ./ (max(data) - min(data)) - 1;
data = data ./ max(abs(data));
[t2f, toneNames, freqMap, f2t] = makeToneMap();

%% phase 1 - check fundamental frequencies

figure
tiledlayout(1,1,'Padding','compact','TileSpacing','compact')
nexttile()
pspectrum(data,fs,'spectrogram','FrequencyResolution',2)
ylim([0,2.5])

%% phase 2 - enter soundfile properties

firstTone = "G4";
lastTone = "F7";

firstInterval = 5; %empty if not present
secondInterval = []; %empty if not present


instrumentName = "xylophone";

filename_new = strjoin([instrumentName firstTone lastTone firstInterval secondInterval], "_");

%% phase 3 - plot tones

[uP, lP] = envelope(data + 0.0001*randn(size(data)), 50, 'peak');
amplitude = max(abs(uP), abs(lP));
threshs = AM_thresholdedRunLength(amplitude, 0.001, 10000, 3000);

figure
tiledlayout(1, 1, 'Padding', 'compact', 'TileSpacing', 'compact')
nexttile()
plot(data)
xlim([1 numel(data)])
hold on
plot(threshs, 'LineWidth', 2)

%% phase 4 - turn info into label data

nTones = (find(toneNames == lastTone) - find(toneNames == firstTone))/2 + 1;

manualLabeling = false;

if manualLabeling
    if(numel(cursor_info) ~= 2*nTones)
        error("Incorrect number of onsets and offsets")
    end
    
    [~,sortIdx] = sort([cursor_info.DataIndex]);
    cursor_info = cursor_info(sortIdx);
    
    onsets = [cursor_info(1:2:end).DataIndex];
    offsets = [cursor_info(2:2:end).DataIndex];
else

    [~, onsets] = findpeaks(double(threshs));
    [~, offsets] = findpeaks(double(fliplr(threshs)));
    offsets = length(threshs) - offsets;
    offsets = sort(offsets);
    
    if(numel(onsets) ~= nTones) || (numel(offsets) ~= nTones)
        error("Incorrect number of onsets and offsets")
    end
end
figure
tiledlayout(1, 1, 'Padding', 'compact', 'TileSpacing', 'compact')
nexttile()
plot(data)
hold on
scatter(onsets,  zeros(1,numel(onsets)),  '+')
scatter(offsets, zeros(1,numel(offsets)), '+')
xlim([1 numel(data)])

nSimultaneousTones = 1 + ~isempty(firstInterval) + ~isempty(secondInterval);

onsetLabels  = repelem(onsets,  nSimultaneousTones);
offsetLabels = repelem(offsets, nSimultaneousTones);

toneLabels = strings(nSimultaneousTones*nTones, 1);

toneLabels(1 : nSimultaneousTones : end) = toneNames(find(toneNames == firstTone) : 2 : find(toneNames == lastTone));
if(nSimultaneousTones >= 2)
    toneLabels(2 : nSimultaneousTones : end) = ...
        toneNames(find(toneNames == firstTone)+2*firstInterval : 2 : find(toneNames == lastTone)+2*firstInterval);
end
if(nSimultaneousTones >= 3)
    toneLabels(3 : nSimultaneousTones : end) = ...
        toneNames(find(toneNames == firstTone)+2*firstInterval+2*secondInterval : 2 : find(toneNames == lastTone)+2*firstInterval+2*secondInterval);
end

%% phase 5 - commit info to files
folderName_dest = "sequencer_data_processed";
fID = fopen(fullfile(".",folderName_dest, strcat(filename_new, ".txt")), 'w');
for k = 1:numel(toneLabels)
    fprintf(fID, '%s\t%i\t%i\t%s\n', toneLabels(k), onsetLabels(k)', offsetLabels(k)', instrumentName);
end
fclose(fID);

movefile(fullfile(".", folderName, filename_original), fullfile(".", folderName_dest, strcat(filename_new, ".wav")));