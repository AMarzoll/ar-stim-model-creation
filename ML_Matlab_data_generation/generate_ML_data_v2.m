%% Takes as input .wav files with sound labels (.txt) and transforms it into input for a machine learning system with sounds already transformed by wavelet convolution.
%% preparation

continuationFlag = true;

nWorkers = 4;
trainSamplePerc = 0.9;


if ~continuationFlag
    curTime = string(datetime('now', 'Format', 'yyyy_MM_dd_HH_mm'));
    logFileName = strjoin(["log_file_", curTime, ".txt"], '');
    fPtr = fopen(logFileName, 'w+');
    fclose(fPtr);
    [phase, iF_start, iF1_start, iF2_start] = deal(1);
else
    files = dir('log_file_*');
    if isempty(files)
        error('No prior session to continue')
    end
    logFileName = files(end).name;
    curTime = regexp(logFileName, '\d+_\d+_\d+_\d+_\d+', 'match');
    curTime = curTime{:};
    [phase, iF_start, iF1_start, iF2_start] = readLogFile(logFileName);
end
soundDataFolders = ["./keyboard_data_processed/", "./sequencer_data_processed/"];
outputFolder = "C:/ML_output";
trainFilename = "ML_traindata_";
evalFilename = "ML_evaldata_";
trainPath = fullfile(outputFolder, strjoin([trainFilename, curTime, ".csv"], ''));
evalPath = fullfile(outputFolder, strjoin([evalFilename, curTime, ".csv"], ''));
labelFiles = arrayfun(@dir, fullfile(soundDataFolders, '*.txt'), 'UniformOutput', false);
labelFiles = vertcat(labelFiles{:});
wavFiles = arrayfun(@dir, fullfile(soundDataFolders, '*.wav'), 'UniformOutput', false);
wavFiles = vertcat(wavFiles{:});

ch2str = @convertCharsToStrings;
[t2f, toneNames, f2t, ~] = makeToneMap();
allTones = toneNames(49:2:215);
segmentDur_s = 0.016666667;
nToleranceSamples = 50; %reject true label if fewer than this number of samples in segment
waveletN = 20;
nWaveletSamples = 4096;
nBackSegments = 5;

tonesToDetect = toneNames(find(toneNames=="C4"):2:find(toneNames=="B4"));
for iT = 1:numel(tonesToDetect)
    f0s(iT) = t2f(tonesToDetect(iT));
end

%define set of "interesting tones" for each f0
%   subharmonics, neighboring frex, harmonics, integer-plus-half harmonics (harmonics of lower tones)
nOvertones = 6;
nHalfF0s = 2;
nIntegerPlusHalfF0s = 4;
nHalfNeighbors = 2;
nNeighboringTones = 12;

%define noise levels for data augmentation
noiseTypes = ["none", "white", "pink"];
noiseMultiplicators = [NaN, 0.05, 0.05];

fileLabels = [];
for iSeg = 0:nBackSegments
    fileLabels = [fileLabels, ...
                  compose("overtone_%u_t%i",          [1 : nOvertones]', repelem(-iSeg, nOvertones)')', ...
                  compose("halfF0_%u_t%i",            [1 : nHalfF0s]', repelem(-iSeg, nHalfF0s)')', ...
                  compose("integerPlusHalfF0_%u_t%i", [1 : nIntegerPlusHalfF0s]', repelem(-iSeg, nIntegerPlusHalfF0s)')', ...
                  compose("halfNeighbor_%u_t%i",      [1 : nHalfNeighbors]', repelem(-iSeg, nHalfNeighbors)')', ...
                  compose("neighboringTones_%u_t%i",  [1 : nNeighboringTones]', repelem(-iSeg, nNeighboringTones)')', ...
                  compose("loudness_factor_t%i", -iSeg), ...
                  ];
end
nFeatPerSeg = numel(fileLabels)/(1+nBackSegments);
fileLabels = [fileLabels, ...
              compose("toneDiff_%u", [1 : 6]')', ...
              "noise_type", ...
              "target_tone_present", ...
              ];

f_overtones = [1 : nOvertones]' * f0s; %includes f0
f_halfF0s = (2.^-[1 : nHalfF0s]') * f0s;
f_integerPlusHalfF0s = (0.5 + [1 : nIntegerPlusHalfF0s]') * f0s;
f_halfNeighbors = f0s .* (2^(1 / 12)).^[-0.5; 0.5];
f_neighboringTones = nan(nNeighboringTones, numel(tonesToDetect));
neighborIndices = [-nNeighboringTones / 2 : nNeighboringTones / 2];
neighborIndices(neighborIndices==0) = [];
for iT = 1:numel(tonesToDetect)
    for iN = 1:nNeighboringTones
        f_neighboringTones(iN, iT) = t2f(toneNames(find(toneNames == tonesToDetect(iT)) + 2 * neighborIndices(iN)));
    end
end

frex = vertcat(f_overtones, f_halfF0s, f_integerPlusHalfF0s, f_halfNeighbors, f_neighboringTones);

%create output file and write header
if ~exist(fullfile(outputFolder), 'dir')
    mkdir(fullfile(outputFolder))
end
if ~continuationFlag
    writetable(table(fileLabels), trainPath, 'WriteVariableNames', false)
    writetable(table(fileLabels), evalPath, 'WriteVariableNames', false)
end

%pre-load all audio files

if ~exist('all_sigs', 'var')
    for iFile = 1:numel(wavFiles)
%     for iFile = 1:2
        fname = fullfile(wavFiles(iFile).folder, wavFiles(iFile).name);
        [all_sigs{iFile}, all_fs(iFile)] = processWavFile(fname, segmentDur_s);
    end
end
%% regular wavelet convolution loop

if isempty(gcp('nocreate'))
    parpool(nWorkers, 'IdleTimeout', 60*24)
end

if phase == 1
    for iF = iF_start:numel(wavFiles)
        writeLogFile(logFileName, 1, iF, 1, 1);
        fprintf('Wavefile %i of %i\n', iF, numel(wavFiles))
        for iN = 1:numel(noiseTypes)
            fprintf('\tNoise type %i of %i\n', iN, numel(noiseTypes))
            %load signal
%             fname = fullfile(wavFiles(iF).folder, wavFiles(iF).name);
%             [signal, fs, scalingFactor] = processWavFile(fname, segmentDu r_s, noiseTypes(iN), noiseMultiplicators(iN));
            signal = all_sigs{iF};
            fs = all_fs(iF);
            
            %add noise
            signal = addNoiseToSignal(signal, noiseTypes(iN), noiseMultiplicators(iN));
            
            %normalize
            scalingFactor = max(abs(signal), [], 1);
            signal = signal ./ scalingFactor;
            
            % read .txt file with labels and generate comparison vectors
            fname = fullfile(labelFiles(iF).folder, labelFiles(iF).name);
            [trueLabelMatrix, rejectLabelMatrix, toneDifferenceMatrix] = processLabelFile(fname, fs, tonesToDetect, numel(signal), segmentDur_s, nToleranceSamples, true);
            
            %create wavelet set
            wavelets(1:size(frex, 1), 1:size(frex, 2)) = Wavelet(0, 1, 1, 1, false); %pre-allocation with placeholder params
            for iX = 1:size(frex, 1)
                for iY = 1:size(frex, 2)
                    wavelets(iX, iY) = Wavelet(frex(iX, iY), waveletN, fs, nWaveletSamples, false);
                end
            end
            
            for iT = 1:numel(tonesToDetect)
                %allocate memory
                res = nan(size(signal, 1), size(signal, 2), size(frex, 1));
                res = complex(res, 0);
                %perform convolutions
                parfor iW = 1:size(wavelets, 1)
                    res(:,:,iW) = conv2(wavelets(iW, iT).wavelet, 1, signal, 'same');
                end
                %multiply each convolution result by the corresponding frequency to balance out power loss at higher
                %frequencies
                f_factor = reshape(frex(:, iT), 1, 1, []);
                outMat = abs(res) .* f_factor;
                %get sum for each convolution segment and store
                outMat = sum(outMat, 1);
                %scale values to make it more ML-friendly
                outMat = log10(outMat);
                outMat = squeeze(outMat);
                %append loudness factor
                outMat = [outMat, log10(scalingFactor)'];
                
                %copy and shift all results such that every row contains the
                %features for the current time segment and its
                %nBackSegments preceding time segments
                outMat(:, nFeatPerSeg+1 : nFeatPerSeg*(nBackSegments+1)) = NaN; %placeholder
                for iNBackSeg = 1:nBackSegments
                    outMat(1+nBackSegments:end, nFeatPerSeg*iNBackSeg+1:nFeatPerSeg*(iNBackSeg+1)) = ...
                        outMat(1+nBackSegments-iNBackSeg:end-iNBackSeg, 1:nFeatPerSeg);
                end
                outMat(1:nBackSegments, :) = [];
                
                %pack convolution results, loudness factor and category labels
                %in one matrix, delete all rows that have been flagged for
                %rejection, then delete rejection flags
                tdm = squeeze(toneDifferenceMatrix(iT,:,:))';
                tdm = tdm(1+nBackSegments:end, :);
                tdm_ph = nan(size(squeeze(toneDifferenceMatrix(iT,:,:))'));
                tdm_ph = tdm_ph(1+nBackSegments:end, :);
                noise = ones(size(trueLabelMatrix(iT,:)'))*iN;
                noise = noise(1+nBackSegments:end, :);
                labels = trueLabelMatrix(iT,:)';
                labels = labels(1+nBackSegments:end, :);
                rejectFlags = rejectLabelMatrix(iT,:)';
                rejectFlags = rejectFlags(1+nBackSegments:end, :);
                outMat = [outMat, tdm, tdm_ph, noise, labels, rejectFlags];
                outMat(outMat(:, end) == 1, :) = [];
                outMat(:, end) = [];
                %balance categories and shuffle randomly
                nCatOneSegments = sum(outMat(:, end)==1);
                catOneIndices = find(outMat(:, end)==1);
                catZeroSegments = find(outMat(:, end)==0);
                randCatZeroIndices = randsample(catZeroSegments, nCatOneSegments); %assumption: more cat 0 samples than cat 1
                outMat = outMat([catOneIndices, randCatZeroIndices],:);
                outMat = outMat(randperm(size(outMat, 1)), :);
                %write to files according to specified ratio
                splitIdx = round(size(outMat, 1) * trainSamplePerc);
                dlmwrite(trainPath, outMat(1:splitIdx, :), '-append')
                dlmwrite(evalPath, outMat(splitIdx+1:end, :), '-append')
            end
        end
    end
end
%% data augmentation trough combining snippets of sound files
writeLogFile(logFileName, 2, 1, iF1_start, iF2_start);

for iF1 = iF1_start:numel(wavFiles)
    fprintf('Wavefile1 %i of %i\n', iF1, numel(wavFiles))
    %load first .wav file
    %fname = fullfile(wavFiles(iF1).folder, wavFiles(iF1).name);
    %[signal_1, fs, scalingFactor_1] = processWavFile(fname, segmentDur_s, 'none', []);
    signal_1 = all_sigs{iF1};
    fs = all_fs(iF1);
    signal_1 = addNoiseToSignal(signal_1, 'none', []);
    scalingFactor_1 = max(abs(signal_1), [], 1);
    signal_1 = signal_1 ./ scalingFactor_1;
    
    %load first label file
    fname = fullfile(labelFiles(iF1).folder, labelFiles(iF1).name);
    [trueLabelMatrix_1, rejectMatrix_1, toneDifferenceMatrix_1] = processLabelFile(fname, fs, tonesToDetect, numel(signal_1), segmentDur_s, nToleranceSamples, true);
    allLabelMatrix_1 = processLabelFile(fname, fs, allTones, numel(signal_1), segmentDur_s, nToleranceSamples, false);
    
    %create wavelet set
    wavelets(1:size(frex, 1), 1:size(frex, 2)) = Wavelet(0, 1, 1, 1, false); %pre-allocation with placeholder params
    for iX = 1:size(frex, 1)
        for iY = 1:size(frex, 2)
            wavelets(iX, iY) = Wavelet(frex(iX, iY), waveletN, fs, nWaveletSamples, false);
        end
    end
    
    for iF2 = iF2_start:numel(wavFiles)
        if iF1 == iF2
            continue;
        end
        if fs ~= all_fs(iF2)
            continue;
        end
        writeLogFile(logFileName, 2, 1, iF1, iF2);
        fprintf('Wavefile2 %i of %i\n', iF2, numel(wavFiles))
        for iN = 1:numel(noiseTypes)
            %only for non-self and unique file combinations
            
            fprintf('\tNoise type %i of %i\n', iN, numel(noiseTypes))
            %load second .wav file
%             fname = fullfile(wavFiles(iF2).folder, wavFiles(iF2).name);
%             [signal_2, fs_2, scalingFactor_2] = processWavFile(fname, segmentDur_s, noiseTypes(iN), noiseMultiplicators(iN));
            signal_2 = all_sigs{iF2};
            signal_2 = addNoiseToSignal(signal_2, noiseTypes(iN), noiseMultiplicators(iN));
            scalingFactor_2 = max(abs(signal_2), [], 1);
            signal_2 = signal_2 ./ scalingFactor_2;
            
            %load second label file
            fname = fullfile(labelFiles(iF2).folder, labelFiles(iF2).name);
            [trueLabelMatrix_2, ~, toneDifferenceMatrix_2] = processLabelFile(fname, fs, tonesToDetect, numel(signal_2), segmentDur_s, nToleranceSamples, true);
            allLabelMatrix_2 = processLabelFile(fname, fs, allTones, numel(signal_2), segmentDur_s, nToleranceSamples, false);
            
            %for each target tone, create artificial tones and an equal number of decoy tones
            for iT = 1:numel(tonesToDetect)
                %identify target tones to combine in signal_1
                targToneSegments_1 = find(trueLabelMatrix_1(iT, :), Inf);
                rejectSegments_1 = find(rejectMatrix_1(iT, :), Inf);
                targToneSegments_1(ismember(targToneSegments_1, rejectSegments_1)) = []; %delete rejected segments from targToneSegments_1
                targToneSegments_1 = targToneSegments_1(targToneSegments_1 > nBackSegments);
                if(isempty(targToneSegments_1))
                    continue;
                end
                anyToneSegments_1    = find(sum(allLabelMatrix_1, 1) > 0, Inf); %segments that contain any tone at all
                distToneSegments_1   = setdiff(anyToneSegments_1, targToneSegments_1); %segments that contain non-target tones
                distToneSegments_1   = datasample(...
                                                  distToneSegments_1(distToneSegments_1 > nBackSegments), ...
                                                  numel(targToneSegments_1), ...
                                                  'replace', false ...
                                                  ); %random sample of non-target tone segments, same size as number of target tone segments
                comboSegments_1      = [targToneSegments_1, distToneSegments_1]; %combine target and non-target segments
                comboSegments_1      = comboSegments_1 - [-nBackSegments:1:0]'; %insert preceding segments
                comboSegments_1      = reshape(comboSegments_1, 1, []);
                comboScalingFactor_1 = scalingFactor_1(comboSegments_1);
                trueLabels           = [ones(1, numel(targToneSegments_1)) zeros(1, numel(distToneSegments_1))];
                toneDifferences_1    = squeeze(toneDifferenceMatrix_1(iT, :, comboSegments_1(1:(nBackSegments+1):end)))';
                
                %identify tones to combine in signal_2
                targToneSegments_2   = find(trueLabelMatrix_2(iT, :), Inf);
                anyToneSegments_2    = find(sum(allLabelMatrix_2, 1) > 0, Inf);
                distToneSegments_2   = setdiff(anyToneSegments_2, targToneSegments_2);
                distToneSegments_2   = datasample(...
                                                  distToneSegments_2(distToneSegments_2 > nBackSegments), ...
                                                  2 * numel(targToneSegments_1), ...
                                                  'replace', false...
                                                  ); %sample as many segments as there are target tone segments in file 1
                comboSegments_2      = distToneSegments_2; %only non-target tones
                comboSegments_2      = comboSegments_2 - [-nBackSegments:1:0]'; %insert preceding segments
                comboSegments_2      = reshape(comboSegments_2, 1, []);
                assert(numel(comboSegments_1) == numel(comboSegments_2))
                comboScalingFactor_2 = scalingFactor_2(comboSegments_2);
                toneDifferences_2    = squeeze(toneDifferenceMatrix_2(iT, :, comboSegments_2(1:(nBackSegments+1):end)))';
                
                comboSignal = signal_1(:, comboSegments_1) + signal_2(:, comboSegments_2);
                comboScalingFactor_sum = max(abs(comboSignal));
                comboSignal = comboSignal ./ comboScalingFactor_sum;
                
                %allocate memory
                res = nan(size(comboSignal, 1), size(comboSignal, 2), size(frex, 1));
                res = complex(res, 0);
                %perform convolutions
                parfor iW = 1:size(wavelets, 1)
                    res(:,:,iW) = conv2(wavelets(iW, iT).wavelet, 1, comboSignal, 'same');
                end
                %multiply each convolution result by the corresponding frequency to balance out power loss at higher
                %frequencies
                f_factor = reshape(frex(:,iT), 1, 1, []);
                outMat = abs(res) .* f_factor;
                %get sum for each convolution segment and store
                outMat = sum(outMat, 1);
                %scale values to make it more ML-friendly
                outMat = log10(outMat);
                outMat = squeeze(outMat);
                %include loudness factor
                totalScalingFactor = geomean([comboScalingFactor_1; comboScalingFactor_2], 1);
                totalScalingFactor = geomean([totalScalingFactor; comboScalingFactor_sum], 1);
                outMat = [outMat, log10(totalScalingFactor)'];
                temp = nan(size(outMat, 1) / (nBackSegments+1), nFeatPerSeg*(nBackSegments+1));
                for iBackSeg = 0:nBackSegments
                    temp(:, 1+(iBackSeg*nFeatPerSeg):nFeatPerSeg*(iBackSeg+1)) = ...
                        outMat(1+iBackSeg:(nBackSegments+1):end, :);
                end
                outMat = temp;
                %pack convolution results, loudness factor and category labels in one matrix
                outMat = [outMat, ...
                          toneDifferences_1, ...
                          toneDifferences_2, ...
                          ones(size(trueLabels'))*iN, ...
                          trueLabels', ...
                          ];
                %shuffle randomly
                outMat = outMat(randperm(size(outMat, 1)), :);
                %write to files according to specified ratio
                splitIdx = round(size(outMat, 1) * trainSamplePerc);
                dlmwrite(trainPath, outMat(1:splitIdx, :), '-append')
                dlmwrite(evalPath, outMat(splitIdx+1:end, :), '-append')
            end
        end
    end
    iF2_start = 1;
end

%% helper functions

function sig = addNoiseToSignal(sig, noiseType, noiseMultiplicator)
    if ~strcmp(noiseType, "none")
        noiseObj = dsp.ColoredNoise(noiseType, size(sig, 1), size(sig, 2));
        noise = noiseObj();
        noise = noiseMultiplicator * (noise - mean(noise, 1)) ./ std(noise, 1);
        sig = sig + noise;
    end
end



function [sig, fs] = processWavFile(filename, segmentDur_s)
    [sig, fs] = audioread(filename);
    if size(sig,2) == 2
        sig = mean(sig,2);
    elseif size(sig,2) > 2
        error("Incorrect audio format");
    end

    %pad for equal segment size
    nSegmentSamples = round(segmentDur_s * fs);
    padding = nSegmentSamples - rem(numel(sig), nSegmentSamples);
    sig(end:end+padding) = 0;
    %add noise, if defined


    %reshape signal into segments and perform some sort of loudness normalization (factor will be kept for ML model)
    sig = reshape(sig, nSegmentSamples, []);
%     scalingFactor = max(abs(sig), [], 1);
%     sig = sig ./ scalingFactor;
end

function [dataLabelMatrix, rejectMatrix, toneDifferenceMatrix] = processLabelFile(filename, fs, tones, nSig, segmentDur_s, nToleranceSamples, differenceFlag)
    rejectTrailingPerc = 0.20; %TODO: make parameter
    fid = fopen(filename);
    dataLabels = textscan(fid, '%[0123456789ABCDEFG#]\t%u\t%u\t%s');
    fclose(fid);
    nSegmentSamples = round(segmentDur_s * fs);
    dataLabelMatrix = false(numel(tones), nSig);
    
    for iT = 1:numel(tones)
        toneIdx = find(dataLabels{1} == tones(iT));
        if ~isempty(toneIdx)
            for iOccurence = 1:numel(toneIdx)
                dataLabelMatrix(iT, dataLabels{2}(toneIdx(iOccurence)):dataLabels{3}(toneIdx(iOccurence))) = true;
            end
        end
    end
    dataLabelMatrix = reshape(dataLabelMatrix, size(dataLabelMatrix, 1), nSegmentSamples, []);
    dataLabelMatrix = sum(dataLabelMatrix, 2);
    dataLabelMatrix(dataLabelMatrix < nToleranceSamples) = 0;
    dataLabelMatrix = logical(squeeze(dataLabelMatrix));
    
    rejectMatrix = false(size(dataLabelMatrix));
    for iT = 1:numel(tones)
        onsets = strfind(dataLabelMatrix(iT, :), [false, true]);
        offsets = strfind(dataLabelMatrix(iT, :), [true, false]);
        segmentLengths = offsets - onsets;
        nRejectSegments = round(rejectTrailingPerc * segmentLengths);
        rejectSegments = arrayfun(@linspace, offsets-nRejectSegments+1, offsets, nRejectSegments, 'UniformOutput', false);
        rejectSegments = [rejectSegments{:}];
        rejectMatrix(iT, rejectSegments) = true;
    end
    
    if differenceFlag
        nMaxSimultaneousTones = 3;
        nSimultaneousTones = numel(dataLabels{2}) / numel(unique(dataLabels{2}));
        toneDifferenceMatrix = nan(numel(tones), nSimultaneousTones, nSig);
        beginSamples = unique(dataLabels{2});
        endSamples = unique(dataLabels{3});
        
        for iT = 1:numel(tones)
            for iST = 1:nSimultaneousTones
                for iSegm = 1:numel(beginSamples)
                    toneIdx = find(dataLabels{2} == beginSamples(iSegm));
                    curTone = dataLabels{1}{toneIdx+iST-1};
                    toneDifferenceMatrix(iT, iST, beginSamples(iSegm):endSamples(iSegm)) = toneDiff(tones(iT), curTone);
                end
            end
        end
        toneDifferenceMatrix = reshape(toneDifferenceMatrix, size(toneDifferenceMatrix, 1), nSimultaneousTones, nSegmentSamples, []);
        failSegments = squeeze(sum(~isnan(toneDifferenceMatrix), 3) < nToleranceSamples);
        toneDifferenceMatrix = reshape(mode(toneDifferenceMatrix, 3), numel(tones), size(toneDifferenceMatrix, 2), []);
        toneDifferenceMatrix(failSegments) = NaN;
        if nSimultaneousTones < nMaxSimultaneousTones
            toneDifferenceMatrix(:, nSimultaneousTones+1:nMaxSimultaneousTones, :) = NaN;
        end
        
        for iST = 1:nMaxSimultaneousTones
            assert(~any((squeeze(dataLabelMatrix(:,:))==0) & (squeeze(toneDifferenceMatrix(:,iST,:))==0), 'all'))
        end
    else
        toneDifferenceMatrix = [];
    end
end

function [phase, iF_start, iF1_start, iF2_start] = readLogFile(logFileName)
    fPtr = fopen(logFileName);
    vals = fscanf(fPtr, '%u\t%u\t%u\t%u');
    fclose(fPtr);
    vals = num2cell(vals);
    [phase, iF_start, iF1_start, iF2_start] = deal(vals{:});
end

function writeLogFile(logFileName, phase, iF_start, iF1_start, iF2_start)
    fPtr = fopen(logFileName, 'w+');
    fprintf(fPtr, '%u\t%u\t%u\t%u', phase, iF_start, iF1_start, iF2_start);
    fclose(fPtr);
end